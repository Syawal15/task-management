## [BEETLEHR] Task Management

Aplikasi web yang dibangun menggunakan Laravel, Inertia, Vue dan Tailwind CSS untuk pengelolaan tugas.

## Objective

Tujuan dari aplikasi ini adalah untuk memudahkan proses manajemen tugas dalam organisasi. Aplikasi ini memungkinkan pengguna untuk membuat, assigning, dan melacak tugas secara efisien.

## Scope

Aplikasi ini mencakup feature utama seperti berikut:

-   Manajemen User
-   Manajemen Role dan Permission
-   Manajemen Tugas
-   Penugasan Tugas
-   Sistem Notifikasi

## Key Features

-   Authentikasi Pengguna
-   Permission-Based Akses Kontrol
-   Fitur CRUD Users dan Task
-   Penugasan tugas ke user terpilih
-   Background email notifikasi menggunakan queue

## Setup Instructions

1. Clone repository nya dari sini
   https://gitlab.com/Syawal15/task-management
2. Copy **`.env.example`** ke **`.env`** dan setup konfigurasi yang di perlukan (seperti database)
3. Jalankan perintah dibawah ini, lalu aplikasi sudah siap digunakan:
    - **`composer install`**
    - **`php artisan key:generate`**
    - **`yarn`**
    - **`php artisan migrate`**
    - **`php artisan db:seed`**
    - **`yarn dev`**

## Dokumentasi API

Untuk dokumentasi API dapat dilihat disini :
https://documenter.getpostman.com/view/17292374/2s9Ykt6f2B#d589db22-1651-4272-bdd5-e29477dadf52
