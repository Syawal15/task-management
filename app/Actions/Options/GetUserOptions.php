<?php

namespace App\Actions\Options;

use App\Models\Status;
use App\Models\User;

class GetUserOptions
{
    public function handle()
    {
        $users = User::get()->pluck('name', 'id');

        return $users;
    }
}
