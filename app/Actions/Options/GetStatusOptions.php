<?php

namespace App\Actions\Options;

use App\Models\Status;

class GetStatusOptions
{
    public function handle()
    {
        $status = Status::get()->pluck('status', 'id');

        return $status;
    }
}
