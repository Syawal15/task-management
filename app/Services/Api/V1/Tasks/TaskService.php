<?php

namespace App\Services\Api\V1\Tasks;

use App\Models\Task;
use App\Notifications\TaskAssigned;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;

class TaskService
{
    public function getData($request)
    {
        $userId = Auth::id();

        $tasks = Task::with('status')
            ->where('created_by_user_id', $userId)
            ->orderBy('id', 'desc')
            ->paginate(10);

        return $tasks;
    }

    public function getDataById($id)
    {
        $task = Task::with('status')->where('id', $id)->first();

        if ($task->created_by_user_id != Auth::id()) {
            throw new AuthorizationException('Unauthorized');
        }

        return $task;
    }

    public function getDataByUser($request)
    {
        $userId = Auth::id();

        $tasks = Task::with('status')
            ->where('assigned_user_id', $userId)
            ->orderBy('id', 'desc')
            ->paginate(10);

        return $tasks;
    }

    public function store($request)
    {
        $input = $request->only(['title', 'description']);
        $input['status_id'] = 1;
        $input['created_by_user_id'] = Auth::id();

        $task = Task::create($input);

        return $task;
    }

    public function update($id, $request)
    {
        $input = $request->only(['title', 'description']);

        try {
            $task = Task::findOrFail($id);

            if ($task->created_by_user_id != Auth::id()) {
                throw new AuthorizationException('Unauthorized');
            }

            if ($task->status_id != 1) {
                throw new \InvalidArgumentException('Task cannot be updated.');
            }

            $task->update($input);

            return $task;
        } catch (ModelNotFoundException $e) {
            throw new \Exception('Task not found', 404);
        }
    }

    public function assignTask($id, $request)
    {
        $input = $request->only(['assigned_user_id']);
        $input['status_id'] = 2;

        try {
            $task = Task::findOrFail($id);

            if ($task->created_by_user_id != Auth::id()) {
                throw new AuthorizationException('Unauthorized');
            }

            if ($task->status_id != 1) {
                throw new \InvalidArgumentException('Task cannot be assigned.');
            }

            $task->update($input);

            if ($task->assignedUser) {
                $task->assignedUser->notify(new TaskAssigned($task));
            }

            return $task;
        } catch (ModelNotFoundException $e) {
            throw new \Exception('Task not found', 404);
        }
    }

    public function completeTask($id)
    {
        try {
            $task = Task::findOrFail($id);

            if ($task->assigned_user_id != Auth::id()) {
                throw new AuthorizationException('Unauthorized');
            }

            if ($task->status_id != 2) {
                throw new \InvalidArgumentException('Task cannot be completed.');
            }

            $task->status_id = 3;
            $task->save();

            return $task;
        } catch (ModelNotFoundException $e) {
            throw new \Exception('Task not found', 404);
        }
    }

    public function delete($id)
    {
        try {
            $task = Task::findOrFail($id);

            if ($task->created_by_user_id != Auth::id()) {
                throw new AuthorizationException('Unauthorized');
            }

            if ($task->status_id != 1) {
                throw new \InvalidArgumentException('Task cannot be deleted.');
            }

            $task->delete();

            return $task;
        } catch (ModelNotFoundException $e) {
            throw new \Exception('Task not found', 404);
        }
    }
}
