<?php

namespace App\Services\Users;

use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserService
{
    public function getData($request)
    {
        $search = $request->search;
        $filter_role = $request->filter_role;

        $query = User::with('roles');

        $query->when(request('search', false), function ($q) use ($search) {
            $q->where('name', 'like', '%' . $search . '%');
        });
        $query->when(request('filter_role', false), function ($q) use ($filter_role) {
            $q->whereHas("roles", function ($qe) use ($filter_role) {
                $qe->where("id", $filter_role);
            });
        });

        return $query->orderBy('id', 'desc')->paginate(10);
    }

    public function store($request)
    {
        $input = $request->only(['name', 'email']);
        $input['password'] = Hash::make($request->password);

        $user = User::create($input);
        $role = Role::findOrFail($request->role_id);
        $user->assignRole($role->name);

        return $user;
    }

    public function update($id, $request)
    {
        $input = $request->only(['name', 'email']);
        if($request->password) {
            $input['password'] = Hash::make($request->password);
        }

        $user = User::findOrFail($id);
        $user->update($input);
        $user->syncRoles($request->role);

        return $user;
    }

    public function delete($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return $user;
    }
}