<?php

namespace App\Http\Controllers\Api\V1\Tasks;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\Api\V1\Tasks\AssignTaskRequest;
use App\Http\Requests\Api\V1\Tasks\TaskRequest;
use App\Http\Resources\Api\V1\Tasks\SubmitTaskResource;
use App\Http\Resources\Api\V1\Tasks\TaskListResource;
use App\Services\Api\V1\Tasks\TaskService;
use Illuminate\Support\Facades\Request;

class TaskController extends ApiBaseController
{
    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    public function index(Request $request)
    {
        try {
            $data = $this->taskService->getData($request);
            $result = new TaskListResource($data);
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }

    public function detail($id, Request $request)
    {
        try {
            $data = $this->taskService->getDataById($id, $request);
            $result = new SubmitTaskResource($data, 'Success Get All Task');
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }

    public function store(TaskRequest $request)
    {
        try {
            $data = $this->taskService->store($request);
            $result = new SubmitTaskResource($data, 'Success Create New Task');
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }

    public function update($id, TaskRequest $request)
    {
        try {
            $data = $this->taskService->update($id, $request);
            $result = new SubmitTaskResource($data, 'Success Update Task');
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }

    public function assignTask($id, AssignTaskRequest $request)
    {
        try {
            $data = $this->taskService->assignTask($id, $request);
            $result = new SubmitTaskResource($data, 'Success Assign Task');
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }

    public function completeTask($id)
    {
        try {
            $data = $this->taskService->completeTask($id);
            $result = new SubmitTaskResource($data, 'Success Complete Task');
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $data = $this->taskService->delete($id);
            $result = new SubmitTaskResource($data, 'Success Delete Task');
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }
}
