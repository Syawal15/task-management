<?php

namespace App\Http\Controllers\Tasks;

use App\Actions\Options\GetStatusOptions;
use Inertia\Inertia;
use App\Actions\Options\GetUserOptions;
use App\Http\Controllers\AdminBaseController;
use App\Http\Requests\Tasks\TaskRequest;
use App\Http\Resources\Tasks\SubmitTaskResource;
use App\Http\Resources\Tasks\TaskListResource;
use App\Services\Tasks\TaskService;
use Illuminate\Http\Request;

class TaskController extends AdminBaseController
{
    public function __construct(TaskService $taskService, GetUserOptions $getUserOptions, GetStatusOptions $getStatusOptions)
    {
        $this->taskService = $taskService;
        $this->getUserOptions = $getUserOptions;
        $this->getStatusOptions = $getStatusOptions;

        $this->title = "Task Management";
        $this->path = "tasks/index";
        $this->data = [
            'user_lists' => $this->getUserOptions->handle()
        ];
    }

    public function listTasks()
    {
        return Inertia::render($this->source . 'tasks/list-task', [
            "title" => 'List Task',
            "additional" => [
                'status_lists' => $this->getStatusOptions->handle()
            ]
        ]);
    }

    public function getData(Request $request)
    {
        try {
            $data = $this->taskService->getData($request);
            $result = new TaskListResource($data);
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }

    public function getDataByUser(Request $request)
    {
        try {
            $data = $this->taskService->getDataByUser($request);
            $result = new TaskListResource($data);
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }

    public function store(TaskRequest $request)
    {
        try {
            $data = $this->taskService->store($request);
            $result = new SubmitTaskResource($data, 'Success Create New Task');
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }

    public function update($id, TaskRequest $request)
    {
        try {
            $data = $this->taskService->update($id, $request);
            $result = new SubmitTaskResource($data, 'Success Update Task');
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }

    public function assignTask($id, TaskRequest $request)
    {
        try {
            $data = $this->taskService->assignTask($id, $request);
            $result = new SubmitTaskResource($data, 'Success Assign Task');
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }

    public function completeTask($id)
    {
        try {
            $data = $this->taskService->completeTask($id);
            $result = new SubmitTaskResource($data, 'Success Complete Task');
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $data = $this->taskService->delete($id);
            $result = new SubmitTaskResource($data, 'Success Delete Task');
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }
}
