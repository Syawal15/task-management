<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Services\Users\UserService;
use App\Actions\Options\GetRoleOptions;
use App\Http\Controllers\AdminBaseController;
use App\Http\Requests\Users\CreateUserRequest;
use App\Http\Requests\Users\UpdateUserRequest;
use App\Http\Resources\Users\UserListResource;
use App\Http\Resources\Users\SubmitUserResource;

class UserController extends AdminBaseController
{
    public function __construct(UserService $userService, GetRoleOptions $getRoleOptions)
    {
        $this->userService = $userService;
        $this->getRoleOptions = $getRoleOptions;

        $this->title = "User Management";
        $this->path = "users/index";
        $this->data = [
            'role_lists' => $this->getRoleOptions->handle()
        ];
    }

    public function getData(Request $request)
    {
        try {
            $data = $this->userService->getData($request);
            $result = new UserListResource($data);
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }

    public function store(CreateUserRequest $request)
    {
        try {
            $data = $this->userService->store($request);
            $result = new SubmitUserResource($data, 'Success create new user');
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }

    public function update($id, UpdateUserRequest $request)
    {
        try {
            $data = $this->userService->update($id, $request);
            $result = new SubmitUserResource($data, 'Success update user');
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $data = $this->userService->delete($id);
            $result = new SubmitUserResource($data, 'Success delete user');
            return $this->respond($result);
        } catch (\Exception $e) {
            return $this->exceptionError($e->getMessage());
        }
    }
}
