<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'status' => 'draft',
            ],
            [
                'status' => 'on_progress',
            ],
            [
                'status' => 'done',
            ],
        ];

        // Create status
        foreach ($data as $key => $value) {
            try {
                $status = Status::updateOrCreate([
                    'id' => $key + 1
                ], [
                    'id' => $key + 1,
                    'status' => $value["status"],
                ]);
            } catch (\Exception $exception) {
                $this->command->info($exception->getMessage());
                // Do something when the exception
            }
        }
    }
}
