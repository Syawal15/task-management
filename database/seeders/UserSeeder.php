<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $super_admin = User::create([
                'name' => 'Super Admin',
                'email' => 'superadmin@boilerplate.com',
                'email_verified_at' => now(),
                'password' => Hash::make('rahasia123')
            ]);
            $super_admin->assignRole('super admin');

            $faker = Faker::create('id_ID');
            for ($i = 0; $i < 10; $i++) {
                $user = User::create([
                    'name' => $faker->name(),
                    'email' => $faker->unique()->safeEmail(),
                    'email_verified_at' => now(),
                    'password' => Hash::make('rahasia123'),
                    'remember_token' => Str::random(10),
                ]);

                $user->assignRole('user');
            }
        } catch (\Exception $exception) {
            $this->command->info($exception->getMessage());
            // Do something when the exception
        }
    }
}
