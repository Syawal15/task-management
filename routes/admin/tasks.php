<?php

use App\Http\Controllers\Tasks\TaskController;

/*
|--------------------------------------------------------------------------
| Users Routes
|--------------------------------------------------------------------------
|
| Here is where you can register user related routes for your application.
|
*/

Route::controller(TaskController::class)->prefix('tasks')->name('tasks.')->group(function () {
    Route::get('/', 'index')->name('index');
    Route::get('get-data', 'getData')->name('getdata');
    Route::post('create-data', 'store')->name('create');
    Route::put('{id}/update-data', 'update')->name('update');
    Route::put('{id}/assign-task', 'assignTask')->name('assignTask');
    Route::put('{id}/complete-task', 'completeTask')->name('completeTask');
    Route::delete('{id}/delete-data', 'delete')->name('delete');
    Route::get('list-tasks', 'listTasks')->name('listTasks');
    Route::get('get-task-user', 'getDataByUser')->name('getTaskUser');
});
