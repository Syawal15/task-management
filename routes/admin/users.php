<?php

use App\Http\Controllers\Users\UserController;

/*
|--------------------------------------------------------------------------
| Users Routes
|--------------------------------------------------------------------------
|
| Here is where you can register user related routes for your application.
|
*/

Route::controller(UserController::class)->prefix('users')->name('users.')->group(function () {
    Route::get('/', 'index')->name('index');
    Route::get('get-data', 'getData')->name('getdata');
    Route::post('create-data', 'store')->name('create');
    Route::put('{id}/update-data', 'update')->name('update');
    Route::delete('{id}/delete-data', 'delete')->name('delete');
});
