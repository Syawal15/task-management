<?php

use App\Http\Controllers\Api\V1\Auth\AuthenticationController;
use App\Http\Controllers\Api\V1\Auth\ForgotPasswordController;

/*
|--------------------------------------------------------------------------
| Audits Routes
|--------------------------------------------------------------------------
|
| Here is where you can register setting related routes for your application.
|
*/

Route::prefix('auth')->group(function () {
    Route::controller(ForgotPasswordController::class)->group(function () {
        Route::post('/password/email', 'sendResetLinkEmail');
    });
    // Route::post('/password/reset', 'ResetPasswordController@reset')->name('api.reset.password');

    Route::controller(AuthenticationController::class)->group(function () {
        Route::post('login', 'login');

        Route::middleware(['JwtMiddleware'])->group(function () {
            Route::get('logout', 'logout');
            Route::post('refresh', 'refresh');
        });
    });
});
