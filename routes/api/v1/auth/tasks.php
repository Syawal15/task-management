<?php

use App\Http\Controllers\Api\V1\Tasks\TaskController;

/*
|--------------------------------------------------------------------------
| Audits Routes
|--------------------------------------------------------------------------
|
| Here is where you can register setting related routes for your application.
|
*/

Route::controller(TaskController::class)->prefix('tasks')->name('api.tasks.')->group(function () {
    Route::get('/', 'index')->name('index');
    Route::post('/', 'store')->name('create');
    Route::get('{id}', 'detail')->name('detail');
    Route::put('{id}', 'update')->name('update');
    Route::patch('{id}/assign-task', 'assignTask')->name('assignTask');
    Route::patch('{id}/complete-task', 'completeTask')->name('completeTask');
    Route::delete('{id}', 'delete')->name('delete');
});
